import { AppConfiguration } from './environment.base';

export const Configuration: AppConfiguration = {
  EndPoint: '/api/v1',
  DataBase: {
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'G3rm@nD@v1dRu1z',
    database: 'torre-tech',
    entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    synchronize: true,
    timezone: 'local',
  },
  Files: {
    TmpFolder: `${__dirname}/../tmp`,
    UploadsFolder: `${__dirname}/../uploads`,
  },
};
